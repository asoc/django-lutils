from setuptools import setup, find_packages

setup(
    name='lutils',
    version='0.1',
    packages=find_packages(),
    url='http://bitbucket.org/asoc/django-lutils',
    license='',
    author='Alex Honeywell',
    author_email='alex.honeywell@gmail.com',
    description=''
)
