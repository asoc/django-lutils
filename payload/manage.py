import os
import sys

if __name__ == "__main__":
    path = os.path.join(os.path.dirname(__file__))
    if path == '.':
        path = os.getcwd()
    if path not in sys.path:
        sys.path.insert(0, path)

    import lutils.environment
    lutils.environment.load_env()
    lutils.environment.current.configure(path)

    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)
    
