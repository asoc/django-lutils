# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Settings'
        db.create_table('lutils_settings', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64)),
            ('value', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('default', self.gf('django.db.models.fields.TextField')()),
            ('use_default', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('include_in_context', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('write_to_file', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('helper', self.gf('django.db.models.fields.TextField')()),
            ('vtype', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('test', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('list_separator', self.gf('django.db.models.fields.CharField')(max_length=8, blank=True)),
            ('list_element_type', self.gf('django.db.models.fields.CharField')(max_length=16, blank=True)),
        ))
        db.send_create_signal('lutils', ['Settings'])


    def backwards(self, orm):
        # Deleting model 'Settings'
        db.delete_table('lutils_settings')


    models = {
        'lutils.settings': {
            'Meta': {'object_name': 'Settings'},
            'default': ('django.db.models.fields.TextField', [], {}),
            'helper': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'include_in_context': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'list_element_type': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'list_separator': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            'test': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'use_default': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'value': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'vtype': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'write_to_file': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        }
    }

    complete_apps = ['lutils']