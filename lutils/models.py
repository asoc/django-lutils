import os

from django.db import models

from lutils import misc

STRING_TYPES = ['str','text',]
NON_CASTABLE_TYPES = STRING_TYPES + ['list',]

class Settings(models.Model):
    key = models.CharField(max_length=64, unique=True)
    value = models.TextField(default='', blank=True)
    default = models.TextField()
    use_default = models.BooleanField(default=True)
    include_in_context = models.BooleanField(default=False)
    write_to_file = models.CharField(max_length=255, blank=True)
    helper = models.TextField()
    vtype = models.CharField(max_length=16)
    test = models.CharField(max_length=255)
    list_separator = models.CharField(max_length=8, blank=True)
    list_element_type = models.CharField(max_length=16, blank=True)
    
    class Meta:
        permissions = misc.model_permissions('settings')
    
    def save(self, *args, **kwargs):
        super(Settings, self).save(*args, **kwargs)
        import lutils.environment
        env = lutils.environment.current
        if env is not None and self.write_to_file != '':
            _file = "%s/autogen/%s" % (env.PATH, self.write_to_file)
            _dir = os.path.dirname(_file)
            if not os.path.exists(_dir):
                os.makedirs(dir)
            f = open(_file, 'w')
            f.write(self.value_as_type())
            f.close()
    
    def value_as_type(self):
        v = self.default if self.use_default else self.value
        if self.vtype in STRING_TYPES:
            return v
        if self.vtype == 'list':
            lst = v.split(self.list_separator)
            if self.list_element_type != 'str':
                tmp = [map(lambda x: Settings.testv(
                    self.list_element_type, x, self.test
                ), el) for el in [lst,]][0]
                lst = tmp
            return lst
        return Settings.testv(self.vtype, v, self.test)
    
    def __unicode__(self):
        return u'%s' % self.key
    
    @classmethod
    def getv(cls, k):
        """Get the value of a setting"""
        s = Settings.objects.get(key=k)
        return s.value_as_type()
    
    @classmethod
    def getvtype(cls, k):
        """Get the variable type of a setting"""
        s = Settings.objects.get(key=k)
        return s.vtype
    
    @classmethod
    def testv(cls, vtype, v, test=None):
        """Make sure the value can be converted to its type and is valid"""
        c = None
        try:
            if vtype not in NON_CASTABLE_TYPES:
                c = globals()['__builtins__'][vtype](v.strip())
            else:
                c = v
        except Exception:
            raise TypeError
        if test is not None and test != '':
            if not eval(test % c):
                raise ValueError
        return c
    
    @classmethod
    def setv(cls, k, v):
        """Save a new value to a setting"""
        s = Settings.objects.get(key=k)
        s.value = Settings.testv(s.vtype, v, s.test)
        s.use_default = False
        s.save()
        return v
    
    @classmethod
    def defaultv(cls, k):
        """Set a setting to use the default value"""
        s = Settings.objects.get(key=k)
        s.use_default = True
        s.save()
    
    @classmethod
    def getcontext(cls):
        return Settings.objects.filter(include_in_context=True)
    
    @classmethod
    def syncv(cls, key, default, helper, vtype, test, context, writetofile,
              listseparator, list_element_type):
        """Create the setting with the default value if it doesn't exist"""
        try:
            s = Settings.objects.get(key=key)
        except Settings.DoesNotExist:
            s = Settings()
            s.key = key
        s.default = default
        s.helper = helper
        s.vtype = vtype
        s.test = test
        s.write_to_file = writetofile
        s.include_in_context = context
        s.list_separator = listseparator
        s.list_element_type = list_element_type
        s.save()
 
