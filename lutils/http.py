from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse

def render_page(template, context, request):
    return render_to_response(
          template,
          context,
          RequestContext(request)
    )
    
def blank(text=''):
    return HttpResponse(content=text)

def redirect(to=None, name=True, anchor=None, *args, **kwargs):
    import lutils.environment

    if lutils.environment.current is None:
        url = to
    else:
        url = to or lutils.environment.current.REL_PATH

    if name and to:
        url = reverse(to, *args, **kwargs)
    if anchor is not None:
        url += '#%s' % str(anchor)
    return HttpResponseRedirect(url)
