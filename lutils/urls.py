import sys

from django.conf.urls import patterns, include, url
from django.contrib import admin

import lutils.environment

_env = lutils.environment.current

_conf = list()

if _env.is_offline():
    _conf.append(url(r'^.*$', '%s.views.offline' % _env.DEFAULT_APP, 'env_offline'))
else:
    if _env.COMMON_ENV:
        _conf.append(url(r'^', include('%s.environments.common' % _env.SRC_PREFIX)))
    if _env.HAS_ROOT_URLS:
        _conf.append(url(r'^', include('%s.environments.%s' % (_env.SRC_PREFIX, _env.NAME))))
    for conf in _env.URLCONFS:
        site_prefix, app, app_prefix = conf
        try:
            _conf.append(url('^%s%s' % (site_prefix, app_prefix), include('%s.%s.urls' % (_env.SRC_PREFIX, app))))
        except ImportError:
            raise ImportError('%s.%s has no urls module' % (_env.SRC_PREFIX, app)), None, sys.exc_info()[2]

if _env.USE_DJANGO_ADMIN:
    admin.autodiscover()
    _conf.insert(0, url(r'^admin', include(admin.site.urls)))

urlpatterns = patterns('', *_conf)
