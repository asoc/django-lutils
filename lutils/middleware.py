from xml.etree.ElementTree import ElementTree
import hashlib

from django.core.exceptions import MiddlewareNotUsed

from lutils.models import Settings

class SettingsMiddleware:
    env = None

    def __init__(self):
        import lutils.environment
        env = lutils.environment.current
        if env is None:
            return

        SettingsMiddleware.env = env

        try:
            currenthash = Settings.getv('settings_hash')
        except Settings.DoesNotExist:
            currenthash = ''
             
        fp = open(SettingsMiddleware.env.SETTINGS_FILE)
        newhash = hashlib.sha1(fp.read()).hexdigest()
        fp.close()
    
        if currenthash != newhash:
            SettingsMiddleware.syncSettings()
            SettingsMiddleware.syncSettingsHash(newhash)
        
        raise MiddlewareNotUsed
    
    @classmethod
    def syncSettingsHash(cls, shash=None):
        if shash == None:
            fp = open(SettingsMiddleware.env.SETTINGS_FILE)
            shash = hashlib.sha1(fp.read()).hexdigest()
            fp.close()
        Settings.setv('settings_hash', shash)
    
    @classmethod
    def syncSettings(cls):
        tree = ElementTree()
        tree.parse(SettingsMiddleware.env.SETTINGS_FILE)
        settings = list(tree.iter('setting'))
        
        for s in settings:
            d = s.find('default')
            h = s.find('helper')
            t = s.find('type')
            v = s.find('test')
            f = s.find('writetofile')
            ls = s.find('listseparator')
            try:
                c = s.find('name').attrib['includeInContext']
            except KeyError:
                c = False
            try:
                letype = s.find('listseparator').attrib['elementType']
            except:
                letype = 'str'
            Settings.syncv(
                key=s.find('name').text, 
                default=d.text if d is not None else '',
                helper=h.text if h is not None else '',
                vtype=t.text if t is not None else 'str',
                test=v.text if v is not None else '',
                context=c,
                writetofile=f.text if f is not None else '',
                listseparator=ls.text if ls is not None else '',
                list_element_type=letype,
            )
        
