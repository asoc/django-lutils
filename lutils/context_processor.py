import re

from lutils import misc
from lutils.models import Settings, STRING_TYPES

def general(request):
    import lutils.environment
    env = lutils.environment.current
    if env is None:
        return {}

    installed_applist = env.site_apps()
    
    applist = []
    auth_req_applist = []
    user_applist = []
    acp_user_applist = []
    if request.user is not None:
        for p in request.user.get_all_permissions():
            if re.match('.+\.%s$' % misc.ACP_GENERAL_PREFIX, p):
                acp_user_applist += [re.sub(r'(.+)\..+$', '\\1', p),]
            elif re.match('.+\.%s$' % misc.GENERAL_PREFIX, p):
                user_applist += [re.sub(r'(.+)\..+$', '\\1', p),]
        
    for app in installed_applist:
        if not env.IS_ADMIN and app in env.ADMIN_ONLY_APPS:
            continue
        name = app.replace('%s.' % env.SRC_PREFIX, '')
        if app in env.AUTH_REQ_APPS:
            auth_req_applist += [name,]
        else:
            applist += [name,]
    
    i = 0
    for app in applist:
        text = ' '.join(app.split('_'))
        applist[i] = [app, text]
        i += 1   
    i = 0
    for app in auth_req_applist:
        text = ' '.join(app.split('_'))
        auth_req_applist[i] = [app, text]
        i += 1

    context = {
        'BASE_TEMPLATE': 'base_%s.html' % env.NAME,
        'SITE_TITLE': env.SITE_TITLE,
        'SITE_DOMAIN': env.SITE_DOMAIN,
        'SITE_URL': env.SITE_URL,
        'OFFLINE': env.is_offline(),
        'WEBMASTER_EMAIL': env.WEBMASTER_EMAIL,
        'DEV_MODE': env.IS_DEV,
        'TEST_MODE': env.IS_TEST,
        'APP_LIST': applist,
        'AUTH_REQ_APP_LIST': auth_req_applist,
        'DEFAULT_APP': env.DEFAULT_APP,
        'CURRENT_APP': env.get_current_app(request.path.strip('/').split('/')),
        'USER_APP_LIST': user_applist,
        'ACP_USER_APP_LIST': acp_user_applist,
    }

    settingsList = Settings.getcontext()
    settingsContext = {}
    for s in settingsList:
        settingsContext['settings_%s' % s.key] = s.value_as_type()
    tmpContext = settingsContext
    for s in settingsList:
        if s.vtype in STRING_TYPES:
            settingsContext['settings_%s' % s.key] = s.value_as_type() % tmpContext
    if tmpContext is not None and len(tmpContext) > 0:
        context = context.update(settingsContext)
    return context
