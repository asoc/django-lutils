import sys
import importlib
import imp
import os
import re

from django.conf import settings, global_settings

current = None
loaded_environments = {}

MODULE_EXTENSIONS = ('.py',)


def get_environments():
    _file, pathname, description = imp.find_module('environments')
    if _file:
        raise ImportError('Invalid Environment Setup')
        # Use a set because some may be both source and compiled.
    return set([os.path.splitext(module)[0]
                for module in os.listdir(pathname)
                if module.endswith(MODULE_EXTENSIONS) and module != '__init__.py'])


def load_env():
    global current
    current = init_env()


def init_env(name=None):
    global loaded_environments

    if name is None:
        try:
            name = os.environ['WHICH_SITE']
        except KeyError:
            raise RuntimeError('System environment variable WHICH_SITE must be set.')

    if name == 'lutils':
        env = BaseEnvironment(name)
    else:
        module_name = 'src.environments.%s' % name
        try:
            importlib.import_module(module_name)
        except ImportError:
            raise RuntimeError('module %s is not importable.' % module_name)

        python_module = sys.modules[module_name]
        try:
            env = python_module.Environment()
        except Exception as error:
            raise RuntimeError('%s is not a valid lutils environment module:\n    %s' % (name, error))

    loaded_environments[name] = env
    return env


def _get_url_from_format(env, url_format):
    return url_format % {
        'proto': env.SITE_PROTOCOL,
        'domain': env.SITE_DOMAIN,
        'port': ':%s' % env.SITE_PORT if env.SITE_PORT else '',
    }


class BaseEnvironment(object):
    SITE_TITLE = ''
    SITE_DOMAIN = ''
    SITE_PORT = ''
    SITE_PROTOCOL = 'http'

    SRC_PREFIX = 'src'
    COMMON_ENV = False

    UPLOAD_PERMS = 0755

    USE_DJANGO_ADMIN = False

    PATH = '/'
    REL_PATH = '/'

    MEDIA_URL_FORMAT = '%(proto)s://media.%(domain)s/'
    STATIC_URL_FORMAT = '%(proto)s://static.%(domain)s/'
    STATICFILES_DIRS = []

    AUTH_DENY_URL = ''

    DEFAULT_APP = ''
    APPS = []
    ADMIN_ONLY_APPS = []
    NON_SITE_APPS = []
    AUTH_REQ_APPS = []

    INSTALLED_APPS = [
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.humanize',
        'django.contrib.markup',
        'south',
        'widget_tweaks',
        'lutils',
    ]

    HAS_ROOT_URLS = False
    URLCONFS = []
    ROOT_URLCONF = 'lutils.urls'

    WEBMASTER_EMAIL = 'nobody@nowhere.com'

    SECRET_KEY = ''
    TIME_ZONE = 'America/Montreal'

    USER_PROFILE = None

    MIDDLEWARE_CLASSES = [
        'django.middleware.common.CommonMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
        'lutils.middleware.SettingsMiddleware',
    ]

    TEMPLATE_CONTEXT_PROCESSORS = [
        'django.contrib.auth.context_processors.auth',
        'django.core.context_processors.static',
        'django.core.context_processors.media',
        'django.core.context_processors.request',
        'django.contrib.messages.context_processors.messages',
        'lutils.context_processor.general',
    ]

    CACHES = {}
    DATABASES = {}
    AUTHENTICATION_BACKENDS = [
        'lutils.backends.EmailAuthBackend',
    ]

    MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

    def set_default(self, attr, value):
        if not hasattr(self, attr):
            setattr(self, attr, value)

    def __init__(self, name):
        self.set_default('SITE_ADMINS', [('Webmaster', self.WEBMASTER_EMAIL)])
        self.set_default('SITE_URL', _get_url_from_format(self, '%(proto)s://%(domain)s%(port)s/'))
        self.set_default('MEDIA_URL', _get_url_from_format(self, self.MEDIA_URL_FORMAT))
        self.set_default('STATIC_URL', _get_url_from_format(self, self.STATIC_URL_FORMAT))

        self.set_default('SETTINGS_FILE', '/'.join((self.PATH, self.SRC_PREFIX, 'settings.xml')))
        self.set_default('OFFLINE_FILE', "/".join((self.PATH, "OFFLINE")))

        self.set_default('IS_ADMIN', name.endswith('_admin'))
        self.set_default('IS_DEV', name.startswith('dev'))
        self.set_default('IS_TEST', name.startswith('test'))
        self.NAME = name

        self.set_default('DEBUG', self.IS_DEV or self.IS_TEST)
        self.set_default('TEMPLATE_DEBUG', self.DEBUG)

        self.set_default('ADMINS', self.SITE_ADMINS)

        if self.DEBUG:
            self.add_context_processors('django.core.context_processors.debug')

        if self.USE_DJANGO_ADMIN:
            self._add_to_list('django.contrib.admin', self.INSTALLED_APPS)
            self.set_default('ADMIN_MEDIA_PREFIX', "".join((self.STATIC_URL, '/admin/')))
        self._add_to_list(self.all_apps(), self.INSTALLED_APPS)

        if self.use_user_profile():
            self.set_default('AUTH_PROFILE_MODULE', self.USER_PROFILE)

        django_defaults = dir(global_settings)
        for s in django_defaults:
            if re.match(r'^[A-Z].*$', s) and not hasattr(self, s):
                setattr(self, s, getattr(global_settings, s))

        if self.CACHES:
            self.MIDDLEWARE_CLASSES.insert(0, 'django.middleware.cache.UpdateCacheMiddleware')
            self.add_middleware('django.middleware.cache.FetchFromCacheMiddleware')

    def is_offline(self):
        if self.IS_DEV or self.IS_TEST:
            return False
        try:
            with open(self.OFFLINE_FILE) as fp:
                return fp.read()
        except IOError:
            pass
        return False

    def use_user_profile(self):
        return self.USER_PROFILE is not None

    def site_apps(self):
        apps = []
        apps.extend(self.APPS)
        apps.extend(self.ADMIN_ONLY_APPS)
        apps.extend(self.AUTH_REQ_APPS)
        return list(set(apps))

    def all_apps(self):
        apps = self.site_apps()
        apps.extend(self.NON_SITE_APPS)
        return list(set(apps))

    def add_apps(self, names):
        self._add_to_list(names, self.APPS)

    def add_admin_apps(self, names):
        self._add_to_list(names, self.ADMIN_ONLY_APPS)

    def add_auth_req_apps(self, names):
        self._add_to_list(names, self.AUTH_REQ_APPS)

    def add_non_site_apps(self, names):
        self._add_to_list(names, self.NON_SITE_APPS)

    def add_middleware(self, names):
        self._add_to_list(names, self.MIDDLEWARE_CLASSES)

    def add_context_processors(self, names):
        self._add_to_list(names, self.TEMPLATE_CONTEXT_PROCESSORS)

    def add_urlconfs(self, confs, site_prefix=''):
        if site_prefix != '':
            site_prefix = '%s/' % site_prefix

        _confs = []
        for conf in confs:
            if not isinstance(conf, list):
                conf = [conf, conf]
            conf.insert(0, site_prefix)
            _confs.append(conf)
        self._add_to_list(_confs, self.URLCONFS)

    def _add_to_list(self, names, to_list):
        if not isinstance(names, list):
            names = [names]
        for name in names:
            if name not in to_list:
                to_list.append(name)

    def configure(self, path):
        env_path = os.path.join(path, self.SRC_PREFIX)
        if env_path not in sys.path:
            sys.path.insert(0, env_path)

        conf = dict()

        attrs = dir(self)
        for s in attrs:
            if re.match(r'^[A-Z].*$', s):
                conf[s] = getattr(self, s)

        conf['ROOT_URLCONF'] = self.ROOT_URLCONF

        settings.configure(**conf)
