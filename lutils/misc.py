import re

GENERAL_PREFIX = 'general'
ACP_GENERAL_PREFIX = 'acp_general'

permissions = (
    (GENERAL_PREFIX, 'app access'),
    (ACP_GENERAL_PREFIX, 'ACP app access'),
    (GENERAL_PREFIX + '_%s', 'model access', True),
    (ACP_GENERAL_PREFIX +'_%s', 'ACP model access', True),
)

def model_permissions(model):
    return map(lambda p: (p[0] % model if len(p) > 2 else p[0], p[1]), permissions)

def make_slug(unsafestr):
    safestr = unsafestr.lower()
    safestr = re.sub(r'\s+', '_', safestr)
    safestr = re.sub(r'\W+', '', safestr)
    return safestr
